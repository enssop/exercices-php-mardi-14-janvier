<!DOCTYPE html>
<!-- Exercice PHP CodeColliders -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<table>
    <tr><td>2</td><td>0</td><td>0</td></tr>
    <tr><td>2</td><td>1</td><td>2</td></tr>
    <tr><td>2</td><td>2</td><td>4</td></tr>
    <tr><td>2</td><td>3</td><td>6</td></tr>
    <tr><td>2</td><td>4</td><td>8</td></tr>
    <tr><td>2</td><td>5</td><td>10</td></tr>
    <tr><td>2</td><td>6</td><td>12</td></tr>
    <tr><td>2</td><td>7</td><td>14</td></tr>
    <tr><td>2</td><td>8</td><td>16</td></tr>
    <tr><td>2</td><td>9</td><td>18</td></tr>
    <tr><td>2</td><td>10</td><td>20</td></tr>
</table>

<?php

/*
    Recréer ci-dessous le tableau ci-dessus à l'aide d'une boucle.
*/

?>
<table>
<!-- écrire le code après ce commentaire -->



<!-- écrire le code avant ce commentaire -->
</table>

</body>
</html>
