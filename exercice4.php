<!DOCTYPE html>
<!-- Exercice PHP CodeColliders -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
    p {
        font-weight: bold;
    }
    </style>
</head>
<body>

<?php
$etat = "je suis liiiiibre !";

$de1 = mt_rand(1, 6);
$de2 = mt_rand(1, 6);

/*
    le joueur lance un dé à 6 faces,
    le résultat est enregistré dans la variable $de1

    si le joueur tombe sur 6 il lance un second dé ($de2), 
    si le second dé tombe sur 6, le joueur part en prison et la 
    variable état affiche : je suis en prison

    Attention: ne pas utiliser echo ou print, seulement modifier la
    valeur des variables
*/
?>
<!-- écrire le code après ce commentaire -->



<!-- écrire le code avant ce commentaire -->
<?php

echo '<p>Dé 1: '.$de1.'</p>';
echo '<p>Dé 2: '.$de2.'</p>';
echo '<p>Etat: '.$etat.'</p>';

?>
</body>
</html>
