<!DOCTYPE html>
<!-- Exercice PHP CodeColliders -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
    p {
        font-weight: bold;
    }
    </style>
</head>
<body>

<?php
$etat = "je suis liiiiibre !";

$de = mt_rand(1, 6);
/*
    le joueur lance un dé à 6 faces,
    le résultat est enregistré dans la variable $de

    si le joueur tombe sur 6 il part en prison :
    dans ce cas, la variable $etat doit avoir la valeur :
    je suis en prison

    Attention: ne pas utiliser echo ou print, seulement modifier la
    valeur des variables
*/
?>
<!-- écrire le code après ce commentaire -->



<!-- écrire le code avant ce commentaire -->
<?php

echo '<p>Dé: '.$de.'</p>';
echo '<p>Etat: '.$etat.'</p>';

?>
</body>
</html>
