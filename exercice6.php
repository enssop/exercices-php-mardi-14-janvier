<!DOCTYPE html>
<!-- Exercice PHP CodeColliders -->
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
    p {
        font-weight: bold;
    }
    .danger {
        color: red;
    }
    .warning {
        color: orange;
    }
    .success {
        color: green;
    }
    </style>
</head>
<body>

<?php

$alerte = "success";

$poids_personne1 = mt_rand(60, 100);
$poids_personne2 = mt_rand(60, 100);

/*
    Deux personnes montent dans un ascenseur, le poids des usagers
    est connu en kg dans les variables $poids_personne1
    et $poids_personne2.
    La charge maximale de l'ascenseur est de 160kg.

    Il vous est demandé d'instancier la variable $poids_total
    contenant le poids total des usagers

    En fonction du poids total et des classes CSS déjà présentes dans le 
    code de l'exercice, utiliser la bonne classe CSS suivant les critères
    suivants :
     - si le poids est plus grand que 160, le texte devient rouge
     - si le poids est plus grand que 140, le texte devient orange
     - si le poids est inférieur ou égal à 140, le texte reste vert

    Attention: ne pas utiliser echo ou print, seulement modifier la
    valeur des variables
*/
?>
<!-- écrire le code après ce commentaire -->



<!-- écrire le code avant ce commentaire -->
<?php

echo '<p class="'.$alerte.'">Poids total = '.$poids_total.'</p>';

?>
</body>
</html>
